#!/bin/sh

trace-cmd record -e vm_pgoff_merge -f 'updated > 0' -e vm_av_merge -f 'same_count > 0 || diff_count > 0 || pgoff_failed_count > 0 || failed_count > 0' "$@"
