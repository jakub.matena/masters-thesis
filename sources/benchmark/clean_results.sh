cd results
    rm *
cd ..

cd external
    cd jemalloc-5.3.0
        rm trace.dat
    cd ..
    cd kcbench-v0.9.5
        rm trace.dat
    cd ..
    cd redis-7.0.2
        rm trace.dat
    cd ..
cd ..

cd spacing-speed-test
    rm trace.dat
cd ..
