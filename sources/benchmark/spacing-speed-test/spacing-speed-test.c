#define _GNU_SOURCE 1

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include "../../include/helper.h"

/*
 * Divides one big block into smaller ones depending
 * on given parameters.
 */
void restore_same(char *start_p, int steps, int block_size, int space_size) {
	char * p2 = start_p + (steps - 1) * block_size;
	char * p3 = start_p + (steps - 1) * (block_size + space_size);

	for(int i = 0; i < steps - 1; ++i) {
		char * out = mremap(p2, block_size, block_size, MREMAP_MAYMOVE | MREMAP_FIXED, p3);
		p2 -= block_size;
		p3 -= block_size + space_size;
	}
}

/*
 * Create blocks with spaces between them.
 * All the blocks have the same AV.
 */
void create_blocks_same(int steps, int block_size, int space_size, char **start_p)
{
	if (*start_p != NULL) {
		restore_same(*start_p, steps, block_size, space_size);
		return;
	}
	
	// Create a big memory block
	char *p1 = mmap(NULL, steps * (block_size + space_size), PROT_READ | PROT_WRITE,
			MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	// Write the memory block
	memset(p1, 'a', steps * (block_size + space_size));
	
	// Create gaps in between
	*start_p = p1;
	p1 += block_size;
	for(int i = 0; i < steps ; ++i) {
		munmap(p1, space_size);
		p1 += space_size + block_size;
	}
}

/*
 * Creates blocks with spaces between them.
 * Each block has a unique AV.
 * Depending on whether this is a first or following
 * round the initial state can be only restored to save time.
 */
void create_blocks_diff(int steps, int block_size, int space_size, char **start_p)
{
	create_blocks_same(steps, block_size, space_size, start_p);
	
	// Unmaps and maps again each block to create blocks with different AVs
	char * p2 = *start_p;
	for(int i = 0; i < steps; ++i) {
		munmap(p2, block_size);
    		char *new_p = mmap(p2, block_size, PROT_READ | PROT_WRITE,
					MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
		memset(new_p, 'a', block_size);
		if (new_p != p2)
			printf("Error \n");
		p2 += block_size + space_size;
	}
}

/* 
 * Tests VMA merging in case of VMAs linked to different AVs.
 * Creates blocks of memory with spaces between them and then moves the blocks together.
 * Blocks will merge into one big block if supported by kernel.
*/
float measure_second_phase(int steps, int block_size, int space_size, char **start_p, bool diff)
{
	clock_t start = clock();
	// Preparing
	if (diff)
		create_blocks_diff(steps, block_size, space_size, start_p);
	else
		create_blocks_same(steps, block_size, space_size, start_p);

	clock_t second_phase = clock();
	// Merging - Move the blocks together, enabling merge if supported by kernel
	char * p2 = *start_p + block_size + space_size;
	char * p3 = *start_p + block_size;
	for(int i = 0; i < steps - 1; ++i) {
		char * out = mremap(p2, block_size, block_size, MREMAP_MAYMOVE | MREMAP_FIXED, p3);
		p2 += block_size + space_size;
		p3 += block_size;
	}
	clock_t end = clock();
	float first = (float)(second_phase - start) / CLOCKS_PER_SEC;
	float second = (float)(end - second_phase) / CLOCKS_PER_SEC;
	float total = (float)(end - start) / CLOCKS_PER_SEC;
	/*printf("First phase time: %f s\n", first);
	printf("Second phase time: %f s\n", second);
	printf("Total time: %f s\n", total);*/
	return second;
}

int main(int argc, char **argv)
{
	int max_per_round = 2000;
	int steps = 25; // Number of blocks
	bool diff = true;
	bool fault = true;
	if (argc >= 2)
		steps = atoi(argv[1]);
	if (argc >= 3)
		diff = atoi(argv[2]);
	printf("steps: %d\n", steps);
	printf("diff: %d\n", diff);
	int block_size = 4096*1;
	int space_size = 4096*1; /* Size of space between blocks */
	float total = 0;
	char *start_p = NULL;
	while(steps > 0) {
		if (steps > max_per_round)
			total += measure_second_phase(max_per_round, block_size, space_size, &start_p, diff);
		else
			total += measure_second_phase(steps, block_size, space_size, &start_p, diff);
		steps -= max_per_round;
	}
	printf("total: %f\n", total);
	return 0;
}
