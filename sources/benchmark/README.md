1. Run prepare_benchmarks.sh (Optionally clean_preparations.sh can be used to delete downloaded files)
2. Install trace-cmd command (Ubuntu: [https://packages.ubuntu.com/search?keywords=trace-cmd](https://packages.ubuntu.com/search?keywords=trace-cmd); Gentoo: emerge dev-util/trace-cmd)
3. Install time command ([https://www.gnu.org/directory/time.html](https://www.gnu.org/directory/time.html)) into /usr/bin/time (Ubuntu: [https://packages.ubuntu.com/search?keywords=time](https://packages.ubuntu.com/search?keywords=time); Gentoo: emerge sys-process/time)
4. Install pkg-config (Ubuntu: [https://packages.ubuntu.com/search?keywords=pkg-config](https://packages.ubuntu.com/search?keywords=pkg-config); Gentoo: dev-util/pkgconf)
5. Install tcl (Ubuntu: [https://packages.ubuntu.com/search?keywords=tcl8.5](https://packages.ubuntu.com/search?keywords=tcl8.5); Gentoo: emerge dev-lang/tcl)
6. Apply patches from benchmark/patches/[modified/mainline] on to kernel source files
7. Build kernel with applied patches
8. Reboot
9. Run run_benchmarks.sh [modified/mainline] [#runs]
10. Repeat from step 6. with other set of patches
11. Optionally clean_results.sh can be used to delete measured results
