Master's thesis

# Documentation
* Resides in documentation directory
* Contains all documentation apart from comments in the code
* Contains thesis itself

# Sources
* Contains supporting source files for this project including all tests and helper programs
* Kernel changes are located in separate git repository: https://gitlab.com/jakub.matena/linux