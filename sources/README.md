#VMA merging in Linux attachment
Individual directories are described lower.

1. analytic - Contains analytic scripts and a special patch to expose some of kernel's memory managment internal structures.
2. benchmark - Contains benchmarks, benchmark scripts, feature patches (modified) and tracing patches for mainline kernel (mainline).
3. clean.sh - Deletes all generated files
4. history - Contains three historical versions of patches
5. include - Shared helper functions for analytic and benchmark.
6. patches - Feature patches including kernel self tests.
7. results - Raw benchmark results data used in the thesis.

## Virtual machine image
There is also an Ubuntu virtual machine for qemu that is prepared to run modified or mainline kernel and also to run benchmarks. It can be downloaded from either of the following sources:  

1. My server: [home.alabanda.cz/thesis/qemu_image.zip](home.alabanda.cz/thesis/qemu_image.zip)  
2. Ulozto: [https://ulozto.cz/tamhle/eQnaM5Tdl8xq#!ZJV5BGR2ZGZjA2IxBQH0MTSxMzMvZKS4BTkaGT1fIaWcrTSuBD==](https://ulozto.cz/tamhle/eQnaM5Tdl8xq#!ZJV5BGR2ZGZjA2IxBQH0MTSxMzMvZKS4BTkaGT1fIaWcrTSuBD==)