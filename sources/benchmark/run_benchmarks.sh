#!/bin/sh

modified=$1
runs=$2

# Set variables based on modified/mainline input
if [ "$modified" == "modified" ]
then
	usage=`pwd`/usage/usage_modified.sh
	trace=`pwd`/trace/trace_modified.sh
	time_results=`pwd`/results/modified_times
	time_sumup=`pwd`/results/modified_times_sumup
	merge_results=`pwd`/results/modified_merges
	merge_sumup=`pwd`/results/modified_merges_sumup
else
	usage=`pwd`/usage/usage_mainline.sh
	trace=`pwd`/trace/trace_mainline.sh
	time_results=`pwd`/results/mainline_times
	time_sumup=`pwd`/results/mainline_times_sumup
	merge_results=`pwd`/results/mainline_merges
	merge_sumup=`pwd`/results/mainline_merges_sumup
fi
usage_nop=`pwd`/usage/usage_nop.sh

uname -r >> $time_sumup
uname -r >> $merge_sumup

# Run performance benchmark x times
for (( i=1; i<=$runs; i++ ))
do
	./run_benchmark.sh $usage_nop no_trace $time_results
done

# Run merge benchmark x times
for (( i=1; i<=$runs; i++ ))
do
	./run_benchmark.sh $usage $trace $merge_results
done

# Sum up the results
./sumup.sh $time_results $modified $runs >> $time_sumup
./sumup.sh $merge_results $modified $runs >> $merge_sumup
