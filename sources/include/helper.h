#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>

int mygetch ( void );

void print_memory (const char *name);

void print_memory_mode (const char *name, const char *mode, int line);

void print_memory_mode_grep (const char *name, const char *mode, void *address, int line);

bool check_vm(void *a, void *b);
