#define _GNU_SOURCE 1
#include <stdlib.h>
#include <stdio.h>
#include <sys/mman.h>


#include "../include/helper.h"

/*
 * Root process is forked and then its child is forked again
 * creating parent, child and grandchild processes. Parent,
 * child and grandchild VMA is printed including its supporting
 * structures anon_vma and anon_vma_chains. Printing is done
 * using patched mremap call that if used correctly only prints
 * the information and does not actually affect the mappings
 * in any way. The patch file is named 0001-Printing.patch.
 */
int main(int argc, char **argv) {
	unsigned long page_size = sysconf(_SC_PAGESIZE);

	// 1.] PHASE ONE - create single mapping and fault it
	char *p1 = mmap(NULL, page_size, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	memset(p1, 'a', page_size);

	// 2.] PHASE TWO - fork and print mapping data using patched mremap()
	int pid = fork();
	if (!pid) {
	// CHILD
		sleep(0.5);
		printf("\n\nCHILD\n");
		mremap(p1, page_size, page_size, MREMAP_MAYMOVE | MREMAP_FIXED, p1);

		// 3.] PHASE THREE
		pid = fork();
		if (!pid) {
			// GRANDCHILD
			sleep(0.5);
			printf("\n\nGRANDCHILD\n");
			mremap(p1, page_size, page_size, MREMAP_MAYMOVE | MREMAP_FIXED, p1);
		} else {
			// CHILD
			printf("\n\nCHILD after second fork\n");
			mremap(p1, page_size, page_size, MREMAP_MAYMOVE | MREMAP_FIXED, p1);
		}
	} else {
		// MAIN
		printf("PARENT after first fork\n");
		mremap(p1, page_size, page_size, MREMAP_MAYMOVE | MREMAP_FIXED, p1);
		sleep(1.5);
		printf("\n\nPARENT after second fork\n");
		mremap(p1, page_size, page_size, MREMAP_MAYMOVE | MREMAP_FIXED, p1);
	}
	return 0;
}
