#!/bin/sh/home/jakub/documents/kcbench-v0.9.5/

test=$1
results=$2
usage=$3
trace=$4

cd jemalloc-5.3.0/
gmake clean > /dev/null
if [ "$trace" = "no_trace" ]
then
    { time gmake $test > /dev/null 2>&1 ; } 2>> $results
else
    $trace gmake $test > /dev/null 2>&1
    sudo $usage >> $results
fi
cd -
