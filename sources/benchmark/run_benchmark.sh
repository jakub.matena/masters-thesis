#!/bin/sh

usage=$1
trace=$2
results=$3

date

cd spacing-speed-test
echo >> $results
echo spacing_speed_test_same >> $results
echo >> $results
./run_sst.sh $results $usage $trace 0
echo >> $results
echo >> $results

echo >> $results
echo spacing_speed_test_diff >> $results
echo >> $results
./run_sst.sh $results $usage $trace 1
echo >> $results
echo >> $results
cd -

cd external

echo >> $results
echo tests_unit >> $results
echo >> $results
bash run_jemalloc.sh tests_unit $results $usage $trace
echo >> $results
echo >> $results

echo >> $results
echo tests_integration >> $results
echo >> $results
bash run_jemalloc.sh tests_integration $results $usage $trace
echo >> $results
echo >> $results

echo >> $results
echo tests_analyze >> $results
echo >> $results
bash run_jemalloc.sh tests_analyze $results $usage $trace
echo >> $results
echo >> $results

echo >> $results
echo tests_stress >> $results
echo >> $results
bash run_jemalloc.sh tests_stress $results $usage $trace
echo >> $results
echo >> $results


echo >> $results
echo redis >> $results
echo >> $results
bash run_redis.sh $results $usage $trace
echo >> $results
echo >> $results


echo >> $results
echo kcbench_times >> $results
echo >> $results
bash run_kcbench.sh $results $usage $trace
echo >> $results
echo >> $results

cd -

date
