\begin{thebibliography}{13}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{\texttt{#1}}
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi: #1}\else
  \providecommand{\doi}{doi: \begingroup \urlstyle{rm}\Url}\fi

\bibitem[community(2022{\natexlab{a}})]{Kernel}
Kernel community.
\newblock Linux kernel code.
\newblock \url{https://www.kernel.org/}, 2022{\natexlab{a}}.

\bibitem[community(2022{\natexlab{b}})]{Wikipedia}
Wikipedia community.
\newblock Wikipedia articles.
\newblock \url{https://www.wikipedia.org/}, 2022{\natexlab{b}}.

\bibitem[Corbet(2010)]{Corbet4}
Jonathan Corbet.
\newblock The case of the overly anonymous anon\_vma.
\newblock \url{https://lwn.net/Articles/383162/}, 2010.

\bibitem[Corbet(2014)]{Corbet1}
Jonathan Corbet.
\newblock An introduction to compound pages.
\newblock \url{https://lwn.net/Articles/619514/}, 2014.

\bibitem[Corbet(2017)]{Corbet3}
Jonathan Corbet.
\newblock Five-level page tables.
\newblock \url{https://lwn.net/Articles/717293/}, 2017.

\bibitem[Corbet(2021)]{Corbet2}
Jonathan Corbet.
\newblock Clarifying memory management with page folios.
\newblock \url{https://lwn.net/Articles/849538/}, 2021.

\bibitem[Duarte(2009{\natexlab{a}})]{Duarte}
Gustavo Duarte.
\newblock Anatomy of a program in memory.
\newblock \url{https://manybutfinite.com/post/anatomy-of-a-program-in-memory/},
  2009{\natexlab{a}}.

\bibitem[Duarte(2009{\natexlab{b}})]{Duarte2}
Gustavo Duarte.
\newblock How the kernel manages your memory.
\newblock
  \url{https://manybutfinite.com/post/how-the-kernel-manages-your-memory/},
  2009{\natexlab{b}}.

\bibitem[Frazier(2022)]{Frazier}
William Frazier.
\newblock Copy on write.
\newblock \url{https://lwn.net/Articles/717293/}, 2022.

\bibitem[Gorman(2004{\natexlab{a}})]{Gorman}
Mel Gorman.
\newblock Understanding the linux virtual memory manager.
\newblock
  \url{https://www.kernel.org/doc/gorman/html/understand/understand006.html},
  2004{\natexlab{a}}.

\bibitem[Gorman(2004{\natexlab{b}})]{Gorman2}
Mel Gorman.
\newblock Understanding the linux virtual memory manager.
\newblock
  \url{https://www.kernel.org/doc/gorman/html/understand/understand014.html},
  2004{\natexlab{b}}.

\bibitem[Richman(2021)]{Richman}
Grant~Seltzer Richman.
\newblock Basic guide to linux mailing lists.
\newblock \url{https://www.grant.pizza/blog/mailing-list-guide/}, 2021.

\bibitem[Zhang(2021)]{Zhang}
Wenbo Zhang.
\newblock Linux kernel vs. memory fragmentation (part i).
\newblock
  \url{https://en.pingcap.com/blog/linux-kernel-vs-memory-fragmentation-1/},
  2021.

\end{thebibliography}
