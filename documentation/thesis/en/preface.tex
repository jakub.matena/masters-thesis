\chapter*{Introduction}


\addcontentsline{toc}{chapter}{Introduction}
The Linux kernel manages virtual address space of each process using \gls{vma} structures to represent mapped ranges. Each area can map a part of a file or be anonymous which makes it basically just a block of usable memory.
\bigbreak

The memory areas can be created, removed, resized, split or merged as a result of \verb|mmap()|, \verb|munmap()|, \verb|mremap()|, \verb|mprotect()| and other syscalls. Ideally, each virtually contiguous range mapping a single linear file segment, or anonymous memory, with single set of attributes such as read-write-protection, would be always represented with a single \gls{vma}. However, for anonymous memory ranges, the current implementation may not merge their VMA structures after non-contiguous ranges become contiguous due to implementation limitations. Thus in processes utilizing \verb|mremap()| heavily this limitation results in extra memory and CPU overhead due to the need to manage larger amounts of \gls{vma} structures. An unaware application can even gradually create lots of mappings and eventually exhaust the mapping limit. Also due to another limitation in \verb|mremap()| implementation, it's impossible to \verb|mremap()| areas that should be contiguous, but consist of multiple VMA structures, in a single syscall.

\bigbreak

This thesis is about low-level details of the kernel's memory management. The target audience are kernel developers and daring students. The reader should already be familiar with the virtual memory concepts including the process memory structure, the address translation and paging, parallelism including locking, system calls and operating systems in general. Understanding the Linux kernel is no easy task because the documentation is scarce and incomplete. Also, getting the community to thoroughly review the code is sometimes close to impossible. The only way leading to a success are therefore incremental changes, debugging and testing to slowly acquire the knowledge and experience needed to get to the bottom of it, however deep the bottom is.

%There are three largely used operating systems: Linux, Windows and macOS. Market division between these \gls{os}s differs based on the hardware we are looking on and how we define the metric. This thesis deals with memory management which is obviously needed for whatever purpose we use the device. However, applications doing unusual memory operations which trigger even the deepest code in an \gls{os} and might benefit from this thesis are usually running on special purpose servers and not on mobile phones, laptops or even desktops. Closest usable metric is therefore public servers reachable via internet, where Unix has 78.8 \% (out of which 38.7 \% is Linux) and Windows has 21.4 \%. Other \gls{os}s have under 0.1 \%, according to W3Techs\footnote{\url{https://w3techs.com/technologies/overview/operating_system} and \url{https://w3techs.com/technologies/details/os-linux}}. This makes Linux one of the most used \gls{os}s or even the most used counting in a possible measuring error.

% What is the problem (human understandable), what people want.
% What is kernel, what are we dealing with, memory management, mappings


\section*{Document structure}

This chapter is a short summary of what you can find in individual chapters. If you already have a good grasp of the kernel, feel free to skip Analysis and continue directly to Design and other chapters.

\subsection*{Analysis}
This chapter briefly introduces Linux as an \gls{os} and its development process. Then, it dives into the development tools necessary for the analysis of memory management from the memory mapping point of view. This contains description of individual functions and structures, as well as more high level point of view of the whole system.

\subsection*{Design}
The design describes changes to the kernel that were necessary to address the current VMA merge shortcomings. This includes the design of individual helper functions, as well as a complex description of the individual patches, including\break \verb|vma_merge()| refactor, page offset update, different \verb|anon_vma| merge, \verb|mremap()| expansion merge and tracing. 

\subsection*{Testing}
Testing describes all tests including merge tests, analytic programs and other specialized tests. The tests are added as an attachment to this thesis.

\subsection*{Results}

This chapter presents all results of this thesis. Specifically, it provides an overview of the hardware and software used for the measurements, speed and successful merge count results, and of course the final summarization of advantages and limitations.

\section*{Problem specification}

\bigbreak

% Current situation, what can be used for the solution
When merging two \gls{vma}s, a number of conditions have to be met. Most importantly, the second area has to follow the first without an empty space between them. This is checked using the sizes of the areas and their start addresses. If the memory is a mapping of a file then also the page offsets in the file have to follow. For example, if the first area contains, in order, pages number 4, 5 and 6, then the second area has to follow with page number 7 and possibly others. And of course both areas must be mapping the same file. Unfortunately, there is no exception for anonymous mappings regarding the page offsets and so this condition is required even when the mapping has nothing to do with files.

\bigbreak

The page offsets of anonymous mappings are initially set equal to the virtual address of the mapping with page bits removed. This is done as the \verb|mmap()| call creates the area. The offset is left unchanged when the area is moved using the \verb|mremap()| call after at least one page in the mapping has already been faulted. Later, when a merge is attempted between this and another \gls{vma}, the page offset may not follow up, because it was not updated after the move operation. Such a \gls{vma} cannot merge with basically any other VMA, which causes unnecessary and long term mapping fragmentation.

\bigbreak

Additionally, because \gls{vma}s can be shared among processes and can be created and removed pretty easily, there is a structure called \gls{av}, which is referenced from a \gls{vma} and also from each page. When merging two \gls{vma}s, their references must point to the same \gls{av}. A VMA can map pages belonging to different AVs only in certain cases, for details see \fullref{sec:what_prevents_merges}. Therefore, if the VMAs point to different AVs, the merge also fails. Unfortunately, in many cases two VMAs that would otherwise be perfect match for a merge do not share the same \gls{av} and cannot be merged.

\bigbreak

The last identified opportunity for a merge is when the \verb|mremap()| call expands an already existing mapping and this growth causes the adjacent gap to disappear. In this case, a merge attempt with the next VMA might succeed, but unfortunately, the current kernel omits the necessary call to \verb|vma_merge()| in such a situation.

%\subsubsection*{Link to sources}
%In mremap.c in copy\_vma() new pgoff is not updated. This causes the vma\_merge() to fail, because it seems VMAs do not follow up. Checking if the anon\_vma are the one and the same is located in the vma\_merge()'s helper function called is\_mergeable\_anon\_vma() and the missed merge call during expansion is located directly in the mremap syscall implementation.

\section*{Implementation brief}

%Short problem analysis, arguing behind server/client scheme. Why it was chosen, why it is not possible to do it all at client. Large data and slow CPU in mobile devices. Some real time data has to be downloaded again for every search, but can be reused on server size, if a lot of requests are made.

%What was supposed to be done and how it was done. Supposed to done was parse the data into database. It was done using databaseCreator etc.

%Testing was done too.

The solution is theoretically quite simple. Update the page offset during\break \verb|mremap()|, modify the \verb|anon_vma| check and call \verb|vma_merge()| after the mapping expansion. Unfortunately nothing in kernel is easy. The page offset is not only saved in the VMA itself, but also in page structs that represent physical pages, specifically its \verb|page→index| field. Updating the page offset therefore implies updating index in all the page structs. This means locating all of them by the means of page tables, which can be walked using the page walk mechanism already implemented in the kernel. Another problem is that these pages might be shared e.g. due to the \gls{cow} mechanism, in which case changing the page offset is not possible as it would change for all the VMAs mapping it and that would affect other processes than the one actually calling \verb|mremap()|. Sharing can be detected by looking at the \gls{av} relations and physical pages themselves.
\bigbreak
Merging two VMAs linked to two different \gls{av}s is also problematic and again requires changes to the page structures and so the pages must not be shared as well. The \gls{av} pointer is stored in \verb|page→mapping| and can be updated using an already existing function. We again use the page walk to get to the page itself.
\bigbreak
The only relatively easy part is the last change, which just means to call\break \verb|vma_merge()| after a mapping is expanded.


%Testing was done in four different ways: 
%\begin{itemize}
%  \item Application was tested using small C++ application. Up to 100 parallel searches were made %to test, if server side can handle multiple requests and to measure speed under heavy load.
%  \item Client side was tested by users to find out, what is the usability of the client %application. How easy it is to use it and so on.
%  \item Data consumption was measured to find out how efficient the application is.
%  \item And finally the application was installed and tested on different Android devices.  
%\end{itemize}

%It is also about, how well does the application meet the expectations and possible expansion in the future.

%\subsection*{Additional}
%Problem with dynamically changing data, changes can make the application fail. Almost impossible to make the solution resistant to changes.