#!/bin/sh

trace-cmd record -e vm_expand_merge -e vm_pgoff_merge -f 'updated > 0' -e vm_av_merge -f 'diff_count > 0 || failed_av_count > 0 || failed_pgoff_count > 0 || same_count > 0' "$@"
