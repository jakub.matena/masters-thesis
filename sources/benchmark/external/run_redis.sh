#!/bin/sh

results=$1
usage=$2
trace=$3

cd redis-7.0.2/
if [ "$trace" = "no_trace" ]
then
    { time ./runtest > /dev/null 2>&1 ; } 2>> $results
else
    $trace ./runtest > /dev/null 2>&1
    sudo $usage >> $results
fi
cd -
