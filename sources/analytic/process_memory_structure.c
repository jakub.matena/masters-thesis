#define _GNU_SOURCE 1
#include <stdlib.h>
#include <stdio.h>
#include <sys/mman.h>

#include "../include/helper.h"

/*
 * Create and print pointers to different data types to examine
 * process memory structure.
 */
int main() {
	char *name = "\\\\./process_memory_structure";

	static int s2 = 45;
	static int s1;
	int a1 = 4;
	int * p1 = (int*)malloc(sizeof(int));
	int * l1 = (int*)malloc(sizeof(int)*64*1024);
	int a2 = a1 * 2;
	int * p2 = (int*)malloc(sizeof(int));
	int * l2 = (int*)malloc(sizeof(int)*32*1024);
	char* m = mmap(NULL, 4096*5, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);

	char* rm = mremap(m+4096*1, 4096*2, 4096*3, MREMAP_MAYMOVE);
	printf("m: %p, rm: %p", m, rm);
	
	print_memory(name);

	printf("a2:\t%p\n", &a2);
	printf("a1:\t%p\n", &a1);
	printf("m:\t%p\n", m);
	printf("rm:\t%p\n", rm);
	printf("l1:\t%p\n", l1);
	printf("l2:\t%p\n", l2);
	printf("p2:\t%p\n", p2);
	printf("p1:\t%p\n", p1);
	printf("s1:\t%p\n", &s1);
	printf("s2:\t%p\n", &s2);
}
