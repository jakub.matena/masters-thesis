#!/bin/sh

results=$1
usage=$2
trace=$3
diff=$4

if [ "$trace" = "no_trace" ]
then
    ./spacing-speed-test 20000 $diff >> $results
else
    $trace ./spacing-speed-test 20000 $diff >> $results
    echo >> $results
    sudo $usage >> $results
fi
echo >> $results
echo >> $results
