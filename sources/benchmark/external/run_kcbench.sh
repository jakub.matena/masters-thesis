#!/bin/sh

results=$1
usage=$2
trace=$3

cd kcbench-v0.9.5/

if [ "$trace" = "no_trace" ]
then
    ./kcbench -s ../linux-5.18 1>> $results
    echo >> $results
else
    $trace ./kcbench -s ../linux-5.18
    echo kcbench_merges >> $results
    echo >> $results
    sudo $usage >> $results
fi
cd -
