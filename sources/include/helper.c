#include "helper.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>

/*
 * Waits for anykey to be pressed
 */
int mygetch ( void ) 
{
    int ch;
    struct termios oldt, newt;
                
    tcgetattr ( STDIN_FILENO, &oldt );
    newt = oldt;
    newt.c_lflag &= ~( ICANON | ECHO );
    tcsetattr ( STDIN_FILENO, TCSANOW, &newt );
    ch = getchar();
    tcsetattr ( STDIN_FILENO, TCSANOW, &oldt );
                
    return ch;
}

/*
 * Prints list of VMAs for a given process name.
 * @name: process name
 * @mode: maps or smaps (more detailed)
 * @address: address value to grep for
 * @line: used to select corrent process if more processes with the same name are running
 */
void print_memory_mode_grep (const char *name, const char *mode, void *address, int line) {
    printf("\n");
    char command[240];
    sprintf(command, "cat /proc/$(ps aux | grep %s | head -n %d | tail -n 1 | awk -F \" \" '{print $2}')/%s | grep -A 25 %lx",
    				name, line, mode, (unsigned long)address);
    system(command);
    printf("\n");
}

void print_memory_mode (const char *name, const char *mode, int line) {
    printf("\n");
    char command[240];
    sprintf(command, "cat /proc/$(ps aux | grep %s | head -n %d | tail -n 1 | awk -F \" \" '{print $2}')/%s", name, line, mode);
    system(command);
    printf("\n");
}

void print_memory(const char *name) {
    print_memory_mode(name, "maps", 1);
}

bool check_vm(void *a, void *b) {
	FILE *fp;
	char *line = NULL;
	size_t len = 0;
	bool success = false;

        fp = fopen("/proc/self/maps", "r");
	if (fp == NULL) {
		printf("Test failed\n");
		return success;
	}

	while(getline(&line, &len, fp) != -1) {
		char *first = strtok(line,"- ");
		void *first_val = (void *) strtol(first, NULL, 16);
		char *second = strtok(NULL,"- ");
		void *second_val = (void *) strtol(second, NULL, 16);
		if (first_val == a && second_val == b) {
			success = true;
			break;
		}
        }
        fclose(fp);
	return success;
}
