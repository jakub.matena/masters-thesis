#!/bin/sh

cd external

wget -O jemalloc-5.3.0.tar.bz2 https://github.com/jemalloc/jemalloc/releases/download/5.3.0/jemalloc-5.3.0.tar.bz2
tar -xvf jemalloc-5.3.0.tar.bz2
cd jemalloc-5.3.0
./configure
make -j$(nproc)
cd ..


wget -O redis-7.0.2.zip https://github.com/redis/redis/archive/refs/tags/7.0.2.zip
unzip redis-7.0.2.zip
cd redis-7.0.2
make -j$(nproc)
cd ..

wget -O v5.18.zip https://github.com/torvalds/linux/archive/refs/tags/v5.18.zip
unzip v5.18.zip

wget -O kcbench-v0.9.5.zip https://gitlab.com/knurd42/kcbench/-/archive/v0.9.5/kcbench-v0.9.5.zip
unzip kcbench-v0.9.5.zip
cd ..

cd spacing-speed-test
make
cd ..
