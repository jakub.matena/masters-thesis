#!/bin/sh

cd analytic
rm process_memory_structure.o process_memory_structure
rm vma_print.o vma_print
cd -

cd benchmark
./clean_results.sh
./clean_preparations.sh
cd -

cd include
rm helper.o
cd -

cd testing
rm tests.o tests
cd -
