#!/bin/sh

sumup_row()
{
	source=$1
	test=$2
	line=$3
	count=$4

	if [ "$line" == "user" ] || [ "$line" == "sys" ] || [ "$line" == "real" ]
	then
		echo -n $line:
		grep -A 14 "$test" "$source" | grep "^$line" | awk -F ' ' '{print $2}' | awk -v count="$count" -F '[ms]' '{sec = sec + $1 * 60 + $2}END{print sec/count}'
	elif [ "$line" == "total" ]
	then
		grep -A 16 "$test" "$source" | grep "^$line" | awk -v count="$count" -F ':' '{total = total + $2}END{print $1 ": " total/count}'
	elif [ "$line" == "run" ]
	then
		for i in {1..8}
		do
			grep -A 20 "$test" "$source" | grep "^Run $i" | awk -v count="$count" -F ':' '{total = total + $2}END{print $1 ": " total/count}'
		done
	else
		grep -A 16 "$test" "$source" | grep "^$line" | awk -v count="$count" -F ':' '{total = total + $2}END{print $1 ": " total/count}'
	fi
}

sumup_test() 
{
	source=$1
	test=$2
	modified=$3
	count=$4

	echo
	echo $test
	if [ "$test" == "spacing_speed_test_same" ] || [ "$test" == "spacing_speed_test_diff" ]
	then
		sumup_row $source $test "total" $count
	elif [ "$test" == "kcbench_times" ]
	then
		sumup_row $source $test "run" $count
		return 0
	elif [ "$test" != "kcbench_merges" ]
	then
		sumup_row $source $test real $count
		sumup_row $source $test user $count
		sumup_row $source $test sys $count
	fi


	sumup_row $source $test "same success" $count
	if [[ "$modified" == "modified" ]]
	then
		sumup_row $source $test "expand success" $count
		sumup_row $source $test "failed AV" $count
		sumup_row $source $test "failed pgoff" $count
		sumup_row $source $test "diff success" $count
	else
		sumup_row $source $test "diff failed" $count
		sumup_row $source $test "pgoff failed" $count
	fi
	sumup_row $source $test "pgoff success" $count
	sumup_row $source $test "failed total" $count
	sumup_row $source $test "success rate" $count
}

# source file name
source=$1
# patched or unpatched results
modified=$2
# number of tests in the source
count=$3

sumup_test $source spacing_speed_test_same $modified $count
sumup_test $source spacing_speed_test_diff $modified $count

sumup_test $source tests_unit $modified $count
sumup_test $source tests_integration $modified $count
sumup_test $source tests_analyze $modified $count
sumup_test $source tests_stress $modified $count
sumup_test $source redis $modified $count

sumup_test $source kcbench_times $modified $count
sumup_test $source kcbench_merges $modified $count



