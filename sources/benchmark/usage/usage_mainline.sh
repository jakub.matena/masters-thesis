#!/bin/sh

# Prints success merge count and failed merge count and percentage of success cases

same_1=$(trace-cmd report 2>/dev/null | grep "merged=[1]" | grep "same_count=1" | wc -l)
same_2=$(trace-cmd report 2>/dev/null | grep "merged=[1]" | grep "same_count=2" | wc -l)
same_3=$(trace-cmd report 2>/dev/null | grep "merged=[1]" | grep "same_count=3" | wc -l)
same=$(($same_1 + 2 * $same_2 + 3 * $same_3))
pgoff=$(trace-cmd report 2>/dev/null | grep "updated=1" | grep "faulted=1" | wc -l)
fail_av_1=$(trace-cmd report 2>/dev/null | grep "failed_count=1" | wc -l)
fail_av_2=$(trace-cmd report 2>/dev/null | grep "failed_count=2" | wc -l)
fail_av_3=$(trace-cmd report 2>/dev/null | grep "failed_count=3" | wc -l)
fail_av=$(($fail_av_1 + 2 * $fail_av_2 + 3 * $fail_av_3))

fail_pgoff_1=$(trace-cmd report 2>/dev/null | grep "pgoff_failed_count=1" | wc -l)
fail_pgoff_2=$(trace-cmd report 2>/dev/null | grep "pgoff_failed_count=2" | wc -l)
fail_pgoff_3=$(trace-cmd report 2>/dev/null | grep "pgoff_failed_count=3" | wc -l)
fail_pgoff=$(($fail_pgoff_1 + 2 * $fail_pgoff_2 + 3 * $fail_pgoff_3))

succ=$(($same + $pgoff))
fail=$(($fail_av + $fail_pgoff))
total=$(($succ + $fail))
echo -n "same success: "
echo $same
echo -n "pgoff success: "
echo $pgoff
echo -n "diff failed: "
echo $fail_av
echo -n "pgoff failed: "
echo $fail_pgoff
echo -n "failed total: "
echo $fail

echo -n "success rate: "
echo "scale=2 ; $succ / $total * 100" | bc
