#!/bin/sh

# Prints memory mappings of a process with given name

name=$1
id=$(ps aux | grep $name | head -n 1 | awk -F " " '{print $2}')
cat /proc/$id/maps
